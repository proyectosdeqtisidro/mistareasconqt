#ifndef CONTEODECOMPLEJIDAD_H
#define CONTEODECOMPLEJIDAD_H

#include <QDialog>

#include <QDialog>
#include "QtSql/QSqlDatabase";
#include "QtSql/qsqlquery.h";
#include "QtSql/QSqlError";
#include "QtSql/QSqlQuery";

namespace Ui {
class ConteoDeComplejidad;
}

class ConteoDeComplejidad : public QDialog
{
    Q_OBJECT

public:
    explicit ConteoDeComplejidad(int opc, QString nombre_bd, QWidget *parent = nullptr);
    ~ConteoDeComplejidad();
    void ModalidadEjemplo();
    void CrearTabla();

private slots:

    void on_btn_Atras_clicked();

    void on_btn_Menu_clicked();

    void on_btn_FAV_clicked();

private:
    Ui::ConteoDeComplejidad *ui;
    QSqlDatabase dbmain;
};

#endif // CONTEODECOMPLEJIDAD_H
