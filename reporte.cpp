#include "reporte.h"
#include "ui_reporte.h"
#include <qpainter.h>
#include <qpdfwriter.h>
#include <qmessagebox.h>
#include <qspinbox.h>
#include <qinputdialog.h>

#include <qcombobox.h>
Reporte::Reporte(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Reporte)
{
    ui->setupUi(this);


    QPdfWriter pdf("C:/Users/user/Desktop/Tamarindo.pdf");
    QPainter painter(&pdf);

    QPixmap pixmap(ui->tableWidget->size());
    ui->tableWidget->render(&pixmap);

    painter.drawPixmap(QRect(2,2,2,2), pixmap);

    QStringList lst;
      lst << "First" << "Second" << "Third" << "Fourth" << "Fifth";
      bool ok = false;
      QString res = QInputDialog::getItem(this, tr( "Application name" ), tr( "Please select an item" ), lst, 1, true, &ok);
      if ( ok )
          ;// user selected an item and pressed OK
      else
          ;// user pressed Cancel
}

Reporte::~Reporte()
{
    delete ui;
}
