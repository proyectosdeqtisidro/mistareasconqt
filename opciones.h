#ifndef OPCIONES_H
#define OPCIONES_H

#include <QDialog>

namespace Ui {
class Opciones;
}

class Opciones : public QDialog
{
    Q_OBJECT

public:
    explicit Opciones(int opc, QString nombre_bd, QWidget *parent = nullptr);
    ~Opciones();
    void ModalidadEjemplo();

private slots:
    void on_btn_Componentes_clicked();

    void on_btn_FAV_clicked();

    void on_btn_Calculos_clicked();

    void on_btn_Inicio_clicked();

private:
    Ui::Opciones *ui;
};

#endif // OPCIONES_H
