#include "complejidad.h"
#include "ui_complejidad.h"
#include <QDebug>
#include <conteodecomplejidad.h>
#include <qobject.h>
#include <opciones.h>
#include <componentes.h>
#include <QMessageBox>
#include <qinputdialog.h>

int opc3;
QString nombre_bd3;

QString buscar;

template <typename... Args, typename Return, typename Class>
void pmf(Return (Class::*)(Args...)) {}

Complejidad::Complejidad(int opc, QString nombre_bd, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Complejidad)
{
    ui->setupUi(this);

    opc3 = opc;
    nombre_bd3 = nombre_bd;

    setWindowFlags(windowFlags() | Qt::CustomizeWindowHint |
                                   Qt::WindowMinimizeButtonHint |
                                   Qt::WindowMaximizeButtonHint |
                                   Qt::WindowCloseButtonHint);

    ui->cB_Componentes->addItems({"Entrada externa", "Salida externa", "Consulta externa", "Archivos lógicos internos", "Archivos de interfaz externos"}); //Elementos asignados al combobox

    ui->tW_Datos->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    dbmain = QSqlDatabase::addDatabase("QSQLITE");
    dbmain.setDatabaseName(nombre_bd3);
    if(dbmain.open()){
        qDebug() << "Base de datos creada";
    }else{
        qDebug() << "Error";
    }

    MostrarDatos();

    if(opc3==2){
        ModalidadEjemplo();
    }
}

Complejidad::~Complejidad()
{
    delete ui;
}

void Complejidad::ModalidadEjemplo(){

    QFont f( "Times New Roman", 10, QFont::Bold);
    ui->lb_Demo->setFont( f);

    ui->lb_Demo->setText("<center>MODO DEMOSTRACIÓN (NO ES EDITABLE)</center> \nEn la tabla puede asignarle un nivel de complejidad a cada uno de los componentes del paso anterior. (Al pulsar"
                         " con el ratón sobre las celdas de la columna Descripción podrá modificar su contenido o eliminar su fila).");
    ui->lb_Demo->setWordWrap(true);

    ui->btn_siguientePaso->setStyleSheet("border: 1px solid rgb(255, 0, 0); border-width: 2px; border-radius: 10px; color: rgb(0,0,0);background-color: rgb(166, 166, 166);font-weight: bold;");

    ui->cB_Componentes->setCurrentIndex(3);

    ui->tW_Datos->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

void Complejidad::MostrarDatos()
{
    QString consulta;
    consulta.append("SELECT * FROM componentes WHERE Componente='" + buscar + "'");
    QSqlQuery consultar;
    consultar.prepare(consulta);

    if(consultar.exec()){
        qDebug() << "Se ha consultado correctamente";
    }else{
        qDebug() << "No se ha consultado correctamente" << consultar.lastError();
    }

    int fila=0;

    ui->tW_Datos->setRowCount(0);

    while(consultar.next()){

        ui->tW_Datos->insertRow(fila);
        ui->tW_Datos->setItem(fila, 0, new QTableWidgetItem(consultar.value(2).toByteArray().constData()));
        ui->tW_Datos->setItem(fila, 1, new QTableWidgetItem(consultar.value(3).toByteArray().constData()));

        QComboBox *combo;
        combo = new QComboBox;
        combo->addItems({"", "Simple", "Media", "Alta"});
        combo->setProperty("row", ui->tW_Datos->rowCount() - 1);
        combo->setProperty("column", 0);

        combo->setCurrentText(consultar.value(3).toByteArray().constData());

        connect(combo, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(OnComboIndexChanged(const QString&)));
        ui->tW_Datos->setCellWidget(fila, 1, combo);

        combo->setCurrentText(consultar.value(3).toByteArray().constData());

        if(opc3==2){
            combo->setEnabled(false);
        }

    }

}

void Complejidad::on_cB_Componentes_currentIndexChanged(int index)
{
    buscar = ui->cB_Componentes->currentText();

    MostrarDatos();
}

void Complejidad::on_tW_Datos_cellClicked(int row, int column)
{
    if(opc3!=2){
    QTableWidgetItem* theItem = ui->tW_Datos->item(row, 0);
    QString decrip = theItem->text();
    qDebug() << decrip;

    bool ok=true;
    bool salir=false;
    int res=0;
    QString mod;

    QSqlQuery editar;

    res = QInputDialog::getText(this, "OPCIONES", "(1)Modicar campo\n(2)Eliminar campo\n(3)Salir", QLineEdit::Normal, "", &ok).toDouble();

    switch(res) {
        case 1:
            mod = QInputDialog::getText(this, "¡MODIFICAR!", "", QLineEdit::Normal, "", &ok);
            editar.prepare("UPDATE componentes SET Descripcion = :desc WHERE Descripcion='" + decrip + "'");
            editar.bindValue(":desc", mod);
            editar.exec();
        break;
        case 2:
        QMessageBox::information(this, tr("¡ELIMINAR!"), tr("Fila eliminada exitosamente"));
            editar.prepare("DELETE FROM componentes WHERE Descripcion ='" + decrip + "'");
            editar.exec();
        break;
    }

    MostrarDatos();
}
}

void Complejidad::OnComboIndexChanged(const QString& text)
{
    QComboBox* combo = qobject_cast<QComboBox*>(sender());

    if (combo)
    {
        QTableWidgetItem* theItem = ui->tW_Datos->item(((ui->tW_Datos->rowCount()-1) - combo->property("row").toInt()), 0);
        QString decrip = theItem->text();

        QSqlQuery actualizar;

        actualizar.prepare("UPDATE componentes SET Complejidad = :cmpl WHERE Descripcion='" + decrip + "'");
        actualizar.bindValue(":cmpl", text);
        actualizar.exec();

        if(actualizar.exec()){
            qDebug() << "Se ha actualizado correctamente";
        }else{
            qDebug() << "No se ha actualizado correctamente" << actualizar.lastError();
        }

    }

}

void Complejidad::on_btn_siguientePaso_clicked()
{
    bool bloquear = false;

    QSqlQuery q;
    q.prepare("SELECT COUNT (*) FROM componentes WHERE Complejidad= ''");
    q.exec();
    int elementos= 0;
    if (q.next()) {
        elementos= q.value(0).toInt();
    }

    if(elementos!=0){
        bloquear = true;
    }

    if(bloquear==true){
        QMessageBox::information(this, tr("ALERTA!"), tr("Debe asignar la complejidad a todos los elementos."));
    }else if(bloquear==false){
        this->close(); //Se cierra esta ventana.

        ConteoDeComplejidad *cdc;
        cdc = new ConteoDeComplejidad(opc3, nombre_bd3, this);
        cdc->show(); //Se abre la ventana para la simulación con varios objetos.
    }
}

void Complejidad::on_bnt_Menu_clicked()
{
    this->close(); //Se cierra esta ventana.

    Opciones *ops;
    ops = new Opciones(opc3, nombre_bd3, this);
    ops->show(); //Se abre la ventana para la simulación con varios objetos.
}

void Complejidad::on_btn_pasoAnterior_clicked()
{
    this->close(); //Se cierra esta ventana.

    Componentes *cmps;
    cmps = new Componentes(opc3, nombre_bd3, this);
    cmps->show(); //Se abre la ventana para la simulación con varios objetos.
}

void Complejidad::on_tB_Info_clicked()
{
    QMessageBox::information(this, tr("¡AYUDA!"), tr("Seleccione de la lista de las categoríasde los\n"
                                                     "componentes para poder cargarlos y seleccionar la\n"
                                                     "complejidad. (Para modificar y eliminar elementos\n"
                                                     "sobre las casillas de la columna Decripción)."));
}
