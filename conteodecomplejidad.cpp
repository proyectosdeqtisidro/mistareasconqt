#include "conteodecomplejidad.h"
#include "ui_conteodecomplejidad.h"
#include <QDebug>
#include <factorajustedevalor.h>
#include <opciones.h>
#include <complejidad.h>

int opc4;
QString nombre_bd4;

QString val[7] = {" ", "Entrada externa", "Salida externa", "Consulta externa", "Archivos lógicos internos", "Archivos de interfaz externos", "Conteo total"};
int valor[5];
int total;
QString cont[7], simpl[7], med[7], alt[7], totl[7];

int contadorElementos(QString columna, QString fila){
    QSqlQuery q;
    q.prepare("SELECT COUNT (*) FROM componentes WHERE " + columna + "='" + fila + "'");
    q.exec();
    int elementos= 0;
    if (q.next()) {
        elementos= q.value(0).toInt();
    }
    return elementos;
}

int contarDeFilas(){
    QSqlQuery q;
    q.prepare("SELECT COUNT (*) FROM conteo_complejidad");
    q.exec();
    int elementos= 0;
    if (q.next()) {
        elementos= q.value(0).toInt();
    }
    return elementos;
}

int contador2(QString comp, QString difil){
    QSqlQuery q;
    q.prepare("SELECT COUNT (*) FROM componentes WHERE Componente='" + comp + "' AND Complejidad='" + difil + "'");
    q.exec();
    int elementos= 0;
    if (q.next()) {
        elementos= q.value(0).toInt();
    }
    return elementos;
}

ConteoDeComplejidad::ConteoDeComplejidad(int opc, QString nombre_bd, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConteoDeComplejidad)
{
    ui->setupUi(this);

    opc4 = opc;
    nombre_bd4 = nombre_bd;

    setWindowFlags(windowFlags() | Qt::CustomizeWindowHint |
                                   Qt::WindowMinimizeButtonHint |
                                   Qt::WindowMaximizeButtonHint |
                                   Qt::WindowCloseButtonHint);

    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

    dbmain = QSqlDatabase::addDatabase("QSQLITE");
    dbmain.setDatabaseName(nombre_bd4);
    if(dbmain.open()){
        qDebug() << "Base de datos creada";
    }else{
        qDebug() << "Error";
    }

    /*
    qDebug() << contadorElementos("Componente", "Entrada externa");
    qDebug() << contadorElementos("Componente", "Salida externa");
    qDebug() << contadorElementos("Componente", "Consulta externa");
    qDebug() << contadorElementos("Componente", "Archivos lógicos internos");
    qDebug() << contadorElementos("Componente", "Archivos de interfaz externos");
    */

    ui->tableWidget->setItem(1, 0, new QTableWidgetItem(QString::number(contadorElementos("Componente", "Entrada externa"))));
    ui->tableWidget->setItem(2, 0, new QTableWidgetItem(QString::number(contadorElementos("Componente", "Salida externa"))));
    ui->tableWidget->setItem(3, 0, new QTableWidgetItem(QString::number(contadorElementos("Componente", "Consulta externa"))));
    ui->tableWidget->setItem(4, 0, new QTableWidgetItem(QString::number(contadorElementos("Componente", "Archivos lógicos internos"))));
    ui->tableWidget->setItem(5, 0, new QTableWidgetItem(QString::number(contadorElementos("Componente", "Archivos de interfaz externos"))));

    ui->tableWidget->setItem(1, 1, new QTableWidgetItem("3 (" + QString::number(contador2("Entrada externa", "Simple"))+")"));
    ui->tableWidget->setItem(1, 2, new QTableWidgetItem("4 (" + QString::number(contador2("Entrada externa", "Media"))+")"));
    ui->tableWidget->setItem(1, 3, new QTableWidgetItem("6 (" + QString::number(contador2("Entrada externa", "Alta"))+")"));

    valor[0] = 3*contador2("Entrada externa", "Simple") + 4*contador2("Entrada externa", "Media") + 6*contador2("Entrada externa", "Alta");
    ui->tableWidget->setItem(1, 4, new QTableWidgetItem(QString::number(valor[0])));

    ui->tableWidget->setItem(2, 1, new QTableWidgetItem("4 (" + QString::number(contador2("Salida externa", "Simple"))+")"));
    ui->tableWidget->setItem(2, 2, new QTableWidgetItem("5 (" + QString::number(contador2("Salida externa", "Media"))+")"));
    ui->tableWidget->setItem(2, 3, new QTableWidgetItem("7 (" + QString::number(contador2("Salida externa", "Alta"))+")"));

    valor[1] = 4*contador2("Salida externa", "Simple") + 5*contador2("Salida externa", "Media") + 7*contador2("Salida externa", "Alta");
    ui->tableWidget->setItem(2, 4, new QTableWidgetItem(QString::number(valor[1])));

    ui->tableWidget->setItem(3, 1, new QTableWidgetItem("3 (" + QString::number(contador2("Consulta externa", "Simple"))+")"));
    ui->tableWidget->setItem(3, 2, new QTableWidgetItem("4 (" + QString::number(contador2("Consulta externa", "Media"))+")"));
    ui->tableWidget->setItem(3, 3, new QTableWidgetItem("6 (" + QString::number(contador2("Consulta externa", "Alta"))+")"));

    valor[2] = 3*contador2("Consulta externa", "Simple") + 4*contador2("Consulta externa", "Media") + 6*contador2("Consulta externa", "Alta");
    ui->tableWidget->setItem(3, 4, new QTableWidgetItem(QString::number(valor[2])));

    ui->tableWidget->setItem(4, 1, new QTableWidgetItem("7 (" + QString::number(contador2("Archivos lógicos internos", "Simple"))+")"));
    ui->tableWidget->setItem(4, 2, new QTableWidgetItem("10 (" + QString::number(contador2("Archivos lógicos internos", "Media"))+")"));
    ui->tableWidget->setItem(4, 3, new QTableWidgetItem("15 (" + QString::number(contador2("Archivos lógicos internos", "Alta"))+")"));

    valor[3] = 7*contador2("Archivos lógicos internos", "Simple") + 10*contador2("Archivos lógicos internos", "Media") + 15*contador2("Archivos lógicos internos", "Alta");
    ui->tableWidget->setItem(4, 4, new QTableWidgetItem(QString::number(valor[3])));

    ui->tableWidget->setItem(5, 1, new QTableWidgetItem("5 (" + QString::number(contador2("Archivos de interfaz externos", "Simple"))+")"));
    ui->tableWidget->setItem(5, 2, new QTableWidgetItem("7 (" + QString::number(contador2("Archivos de interfaz externos", "Media"))+")"));
    ui->tableWidget->setItem(5, 3, new QTableWidgetItem("10 (" + QString::number(contador2("Archivos de interfaz externos", "Alta"))+")"));

    valor[4] = 5*contador2("Archivos de interfaz externos", "Simple") + 7*contador2("Archivos de interfaz externos", "Media") + 10*contador2("Archivos de interfaz externos", "Alta");
    ui->tableWidget->setItem(5, 4, new QTableWidgetItem(QString::number(valor[4])));

    total = valor[0] + valor[1] + valor[2] + valor[3] + valor[4];
    ui->tableWidget->setItem(6, 4, new QTableWidgetItem(QString::number(total)));

    ui->tableWidget->setItem(6, 0, new QTableWidgetItem(""));
    ui->tableWidget->setItem(6, 1, new QTableWidgetItem(""));
    ui->tableWidget->setItem(6, 2, new QTableWidgetItem(""));
    ui->tableWidget->setItem(6, 3, new QTableWidgetItem(""));


    CrearTabla();


        QSqlQuery insertar_db;
        insertar_db.prepare("INSERT INTO conteo_complejidad(Valor,Conteo,Simple, Media, Alta, Total)"
                            "VALUES (:val,:cont,:simpl,:med,:alt,:totl)");

    if(contarDeFilas()<6){
        for(int i=1; i<7; i++){
            cont[i] = ui->tableWidget->item(i,0)->text();
            simpl[i] = ui->tableWidget->item(i,1)->text();
            med[i] = ui->tableWidget->item(i,2)->text();
            alt[i] = ui->tableWidget->item(i,3)->text();
            totl[i] = ui->tableWidget->item(i,4)->text();

            insertar_db.bindValue(":val", val[i]);
            insertar_db.bindValue(":cont", cont[i]);
            insertar_db.bindValue(":simpl", simpl[i]);
            insertar_db.bindValue(":med", med[i]);
            insertar_db.bindValue(":alt", alt[i]);
            insertar_db.bindValue(":totl", totl[i]);

            if(insertar_db.exec()){
                qDebug() << "Datos ingresados a la tabla";
            }else{
                qDebug() << "Error al ingresar los datos" << insertar_db.lastError();
            }
        }
    }

    QSqlQuery actualizar;
    for(int i=1; i<7; i++){
        actualizar.prepare("UPDATE conteo_complejidad SET Valor=:val, Conteo=:cont, Simple=:simp, Media=:med, Alta=:alt, Total=:totl WHERE id= '" + QString::number(i) + "'");
        cont[i] = ui->tableWidget->item(i,0)->text();
        simpl[i] = ui->tableWidget->item(i,1)->text();
        med[i] = ui->tableWidget->item(i,2)->text();
        alt[i] = ui->tableWidget->item(i,3)->text();
        totl[i] = ui->tableWidget->item(i,4)->text();

        actualizar.bindValue(":val", val[i]);
        actualizar.bindValue(":cont", cont[i]);
        actualizar.bindValue(":simpl", simpl[i]);
        actualizar.bindValue(":med", med[i]);
        actualizar.bindValue(":alt", alt[i]);
        actualizar.bindValue(":totl", totl[i]);

        if(actualizar.exec()){
            qDebug() << "Se ha actualizado";
        }else{
            qDebug() << "Error act" << insertar_db.lastError();
        }
    }

    if(opc4==2){
        ModalidadEjemplo();
    }

}

ConteoDeComplejidad::~ConteoDeComplejidad()
{
    delete ui;
}

void ConteoDeComplejidad::CrearTabla(){ //Creación de la tabla
    QString consulta;
    consulta.append("CREATE TABLE IF NOT EXISTS conteo_complejidad("
                    "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                    "Valor VARCHAR(30),"
                    "Conteo VARCHAR (50),"
                    "Simple VARCHAR (50),"
                    "Media VARCHAR (50),"
                    "Alta VARCHAR (50),"
                    "Total VARCHAR (50)"
                    ");");

    QSqlQuery crear;
    crear.prepare(consulta);
    if(crear.exec()){
        qDebug() << "Tabla creada";
    }else{
        qDebug() << "Tabla no creada" << crear.lastError();
    }
}

void ConteoDeComplejidad::ModalidadEjemplo(){

    QFont f( "Times New Roman", 10, QFont::Bold);
    ui->lb_Demo->setFont( f);

    ui->lb_Demo->setText("<center>MODO DEMOSTRACIÓN (NO ES EDITABLE)</center> \nAquí se cuentan cuantos componentes hay por categoría y por complejidad, lo cual se muestra"
                         " en paréntesis; cada una de estas cantidades se multiplica por un factor dado y se suman en la columna final a la derecha.");
    ui->lb_Demo->setWordWrap(true);

    ui->btn_FAV->setStyleSheet("border: 1px solid rgb(0, 255, 0); border-width: 2px; border-radius: 10px; color: rgb(0,0,0);background-color: rgb(166, 166, 166);font-weight: bold;");

}

void ConteoDeComplejidad::on_btn_Atras_clicked()
{
    this->close(); //Se cierra esta ventana.

    Complejidad *cmjs;
    cmjs = new Complejidad(opc4, nombre_bd4, this);
    cmjs->show(); //Se abre la ventana para la simulación con varios objetos.
}

void ConteoDeComplejidad::on_btn_Menu_clicked()
{
    this->close(); //Se cierra esta ventana.

    Opciones *ops;
    ops = new Opciones(opc4, nombre_bd4, this);
    ops->show(); //Se abre la ventana para la simulación con varios objetos.
}

void ConteoDeComplejidad::on_btn_FAV_clicked()
{
    this->close(); //Se cierra esta ventana.

    FactorAjusteDeValor *fav;
    fav = new FactorAjusteDeValor(opc4, nombre_bd4, this);
    fav->show(); //Se abre la ventana para la simulación con varios objetos.
}
